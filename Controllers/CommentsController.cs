using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using MvcMovie.Models;
using Microsoft.EntityFrameworkCore;

namespace MvcMovie.Controllers
{
    public class CommentsController : Controller
    {
        private AppDbContext _context;
        public CommentsController(AppDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(int Id)
        {
            var movie = await _context.Movies.Where(m => m.Id == Id).Include(m => m.Comments).FirstAsync();
            return PartialView("~/Views/Movie/ListComments.cshtml", movie);
        }

        public async Task<IActionResult> Save(Comments model)
        {
            if (ModelState.IsValid)
            {
                model.Date = DateTime.Now.Date.ToShortDateString();
                _context.Comments.Add(model);
                await _context.SaveChangesAsync();
                return Redirect("/Movie/Details/" + model.MovieId);
            }
            return Redirect("/Movie/Index");
        }
    }
}
